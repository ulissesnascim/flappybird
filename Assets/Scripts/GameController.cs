﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController instance;
    public GameObject gameOverText;
    public Text scoreText;
    public bool gameOver = false;
    public float scrollSpeed = -1.5f;
    public int difficultyMode;
    public int deltaDifficulty = 0;

    private int priorDifficulty = 1;
    private int score = 0;

    GameObject gameController;
    ColumnPool columnPool;

    GameObject fastForward;
    GameObject flashBack;
    FastForwardFlash fastForwardFlash;

    // Use this for initialization
	void Awake ()
    {
		if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
	}

    void Start()
    {
        gameController = GameObject.Find("GameController");
        columnPool = gameController.GetComponent<ColumnPool>();
        fastForward = GameObject.Find("FastForward");
        flashBack = GameObject.Find("FlashBack");
    }

    // Update is called once per frame
    void Update ()
    {


        if (gameOver == true && Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            gameOver = false;
        }


        //changes difficulty according to current score
        if (columnPool.owlActiveFlag == false)
        {
            if (score >= 2 && score < 4)
                difficultyMode = Mathf.Max(1, 2 - deltaDifficulty);
            if (score >= 5 && score < 7)
                difficultyMode = Mathf.Max(1, 3 - deltaDifficulty);
            if (score >= 8 && score < 11)
                difficultyMode = Mathf.Max(1, 4 - deltaDifficulty);
            else if (score >= 12)
                difficultyMode = Mathf.Max(1, 5 - deltaDifficulty);
        }

        //flashes forward or backward cues when difficulty changes
        if (priorDifficulty < difficultyMode)
        {
            fastForward.GetComponent<FastForwardFlash>().flashing = true;
        }

        if (priorDifficulty > difficultyMode)
        {
            flashBack.GetComponent<FastForwardFlash>().flashing = true;
        }

        priorDifficulty = difficultyMode;

    }

    public void BirdScored()
    {
        if (gameOver == true)
        {
            return;
        }
        score++;
        
        scoreText.text = "Score: " + score.ToString();
       
    }

    public void BirdDied()
    {
        gameOverText.SetActive(true);
        gameOver = true;
        difficultyMode = 1;
    }


}
