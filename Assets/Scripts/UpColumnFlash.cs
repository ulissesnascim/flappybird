﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpColumnFlash : MonoBehaviour
{

    //private int maxBlinks = 3;
    private float flashingLive = 0.7f;
    private int blinks = 0;
    private SpriteRenderer spriteRenderer;
    private float timer = 0;
    private bool flashing = false;

    // Use this for initialization
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {

            timer += Time.deltaTime;

            if (timer < flashingLive)
            {
                spriteRenderer.color = new Color(255, 255, 255, 50);
            }
            else
            {
                spriteRenderer.color = new Color(255, 255, 255, 0);
                if (timer > 2.5 * flashingLive)
                {
                    timer = 0;
                    blinks += 1;
                }
            }

        }

    }
