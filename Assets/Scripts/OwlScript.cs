﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlScript : MonoBehaviour {


    private SpriteRenderer spriteRenderer;
    GameObject gameController;
    ColumnPool columnPool;


    // Use this for initialization
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        gameController = GameObject.Find("GameController");
        columnPool = gameController.GetComponent<ColumnPool>();
    }
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < -11f)
        {
            Destroy(this.gameObject);
        }
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        StartCoroutine(OwlEffect());
        spriteRenderer.color = new Color (255, 255, 255, 0);

    }

    private IEnumerator OwlEffect()
    {

        columnPool.owlActiveFlag = true;

        yield return new WaitForSeconds(7);

        columnPool.owlActiveFlag = false;

    }
}
