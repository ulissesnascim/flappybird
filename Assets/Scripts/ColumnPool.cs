﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnPool : MonoBehaviour {
    public int ColumnPoolSize = 8;
    public GameObject columnPrefab;
    public GameObject bird;

    public float spawnRate = 4f;
    public float scoreSize = 2.7f;
    public float columnMin = -2f;
    public float columnMax = 2f;
    public int currentColumn = 0;

    public float probabilityPowerUp = 0.1f;
    public bool powerUpAppearing = false;
    public bool owlActiveFlag = false;
    public GameObject owlPrefab;
    public GameObject flashBackPrefab;
    public GameObject slowMoPrefab;

    private GameObject[] columns;
    private GameObject[] upColumn;
    private SpriteRenderer upColumnSpriteRenderer;
    private int upColumnCounter = 0;
    private GameObject[] downColumn;
    private BoxCollider2D[] scoreTrigger;
    private Vector2 objectPoolPosition = new Vector2(-10f, -25f);
    private float timeSinceLastSpawned;
    private float timeFirstSpawn = 0f;
    private bool firstSpawnFlag = false;
    private float spawnXPosition = 10f;
    private float spawnYPosition = 0f;
    private float[] deltaScoreSize;

    private Vector2 powerUpPosition;

    private GameObject owlPowerUp;
    private float owlDeltaScore = 0;
    private bool[] owlScoreChanged;
    
    private GameObject flashBackPowerUp;
    private GameObject slowMoPowerUp;

    private GameObject[] powerUpArray;


    // Use this for initialization
    void Start ()
    {
        columns = new GameObject[ColumnPoolSize];
        upColumn = new GameObject[ColumnPoolSize];
        downColumn = new GameObject[ColumnPoolSize];
        scoreTrigger = new BoxCollider2D[ColumnPoolSize];
        deltaScoreSize = new float[ColumnPoolSize];

        owlScoreChanged = new bool[5];

        for (int i = 0; i < ColumnPoolSize; i++)
        {
            columns[i] = (GameObject)Instantiate(columnPrefab, objectPoolPosition, Quaternion.identity);
            scoreTrigger[i] = columns[i].GetComponent<BoxCollider2D>();
        }

        upColumn = GameObject.FindGameObjectsWithTag("UpColumn");
        downColumn = GameObject.FindGameObjectsWithTag("DownColumn");

        
    }


    // Update is called once per frame
    void Update()
    {

        timeSinceLastSpawned += Time.deltaTime;

        powerUpArray = GameObject.FindGameObjectsWithTag("PowerUp");

        if (powerUpArray == null)
        {
            powerUpAppearing = false;
        }

        else
        {
            foreach (GameObject aux in powerUpArray)
            {
                if (bird.transform.position.x < aux.transform.position.x)
                {
                    powerUpAppearing = true;
                    break;
                }

                else
                    powerUpAppearing = false;
            }
        }


        //changes column spawn rate and score size according to difficulty
        switch (GameController.instance.difficultyMode)
        {
            case 1:
                spawnRate = 4f;
                scoreSize = 2.7f;
                break;
            case 2:
                spawnRate = 3.7f;
                scoreSize = 2.5f;
                break;
            case 3:
                spawnRate = 3.3f;
                scoreSize = 2.3f;
                break;
            case 4:
                spawnRate = 3.0f;
                scoreSize = 2.15f;
                break;
            case 5:
                spawnRate = 2.5f;
                scoreSize = 2.0f;
                break;
        }


        //columns flash when owl power up is active + activates widened gaps between columns
        if (owlActiveFlag == true)
        {
            foreach (GameObject aux in upColumn)
                aux.GetComponent<UpColumnFlash>().enabled = true;

            owlDeltaScore = 0.2f;
        }
        else
        {
            foreach (GameObject aux in upColumn)
            {
                aux.GetComponent<UpColumnFlash>().enabled = false;
                aux.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
            }

            owlDeltaScore = 0f;
        }


        //spawns first column
        if (GameController.instance.gameOver == false && timeSinceLastSpawned >= timeFirstSpawn && firstSpawnFlag == false)
        {
            firstSpawnFlag = true;
            timeSinceLastSpawned = 0;
            spawnYPosition = Random.Range(columnMin, columnMax);
            spawnXPosition = 10f;
            columns[currentColumn].transform.position = new Vector2(spawnXPosition, spawnYPosition);
            currentColumn++;
            if (currentColumn >= ColumnPoolSize)
                currentColumn = 0;
        }

        //spawns columns after the first
        if (GameController.instance.gameOver == false && timeSinceLastSpawned >= spawnRate && firstSpawnFlag == true)
        {
            timeSinceLastSpawned = 0;
            spawnYPosition = Random.Range(columnMin, columnMax);
            columns[currentColumn].transform.position = new Vector2(spawnXPosition, spawnYPosition);

            //spawns power ups
            if (powerUpAppearing == false && GameController.instance.difficultyMode >= 2)
            {
                if (Random.value < probabilityPowerUp)
                {
                    probabilityPowerUp = 0.1f;
                    powerUpPosition = new Vector2(spawnXPosition - 1.5f, Random.Range(Mathf.Min(spawnYPosition + 4.5f, 7.3f), Mathf.Max(spawnYPosition - 4.5f, 1.225278f)));

                    if (owlActiveFlag == true)
                    {
                        if (Random.value < 0.5f)
                        {
                            slowMoPowerUp = (GameObject)Instantiate(slowMoPrefab, powerUpPosition, Quaternion.identity);
                        }
                        else
                        {
                            flashBackPowerUp = (GameObject)Instantiate(flashBackPrefab, powerUpPosition, Quaternion.identity);
                        }
                    }

                    else
                    {
                        float powerUpRandomizer = Random.value;

                        if (powerUpRandomizer < 1 / 3f)
                        {
                            owlPowerUp = (GameObject)Instantiate(owlPrefab, powerUpPosition, Quaternion.identity);
                        }
                        else if (powerUpRandomizer < 2 / 3f)
                        {
                            flashBackPowerUp = (GameObject)Instantiate(flashBackPrefab, powerUpPosition, Quaternion.identity);
                        }
                        else
                            slowMoPowerUp = (GameObject)Instantiate(slowMoPrefab, powerUpPosition, Quaternion.identity);

                    }


                }
                else
                {
                    if (GameController.instance.difficultyMode > 3)
                        probabilityPowerUp += 0.15f;
                    else
                        probabilityPowerUp += 0.1f;
                }
            }


            currentColumn++;
            if (currentColumn >= ColumnPoolSize)
                currentColumn = 0;


        }


        //introduces harder scores by tightening gaps between pillars, applies owl power up easier score effect
        for (int i = 0; i < ColumnPoolSize; i++)
        {
            deltaScoreSize[i] = scoreTrigger[i].size.y - (scoreSize + owlDeltaScore);
        }

        for (int i = 0; i < ColumnPoolSize; i++)
        {
            scoreTrigger[i].size = new Vector2(scoreTrigger[i].size.x, scoreSize + owlDeltaScore);
        }

        for (int i = 0; i < ColumnPoolSize; i++)
        {
            upColumn[i].transform.position = new Vector3(upColumn[i].transform.position.x, upColumn[i].transform.position.y - deltaScoreSize[i] / 2, upColumn[i].transform.position.z);
        }


        for (int i = 0; i < ColumnPoolSize; i++)
        {
            downColumn[i].transform.position = new Vector3(downColumn[i].transform.position.x, downColumn[i].transform.position.y + deltaScoreSize[i] / 2, downColumn[i].transform.position.z);
        }

    }
}
