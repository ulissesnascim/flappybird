﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMoScript : MonoBehaviour {

    private SpriteRenderer spriteRenderer;
    GameObject gameController;
    ColumnPool columnPool;

    GameObject bird;

    private bool slowMoActive;
    private bool effectApplied;
    private bool effectDisabled;


    // Use this for initialization
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        gameController = GameObject.Find("GameController");
        columnPool = gameController.GetComponent<ColumnPool>();
        bird = GameObject.Find("Bird");
    }

    // Update is called once per frame
    void Update()
    {

        if (transform.position.x < -11f && slowMoActive == false)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        spriteRenderer.color = new Color(255, 255, 255, 0);
        StartCoroutine(SlowMoEffect());
        return;

    }

    private IEnumerator SlowMoEffect()
    {
        slowMoActive = true;

        if (!effectApplied)
        {
            bird.transform.localScale = new Vector3(0.7f, 0.7f, 1);
            bird.GetComponent<Rigidbody2D>().gravityScale = 1.3f;
            bird.GetComponent<BirdController>().upForce = 250f;
            effectApplied = true;
        }

        yield return new WaitForSeconds(7);

        if (!effectDisabled)
        {
            bird.transform.localScale = new Vector3(1f, 1f, 1);
            bird.GetComponent<Rigidbody2D>().gravityScale = 1f;
            bird.GetComponent<BirdController>().upForce = 200f;
            effectDisabled = true;
        }

        slowMoActive = false;
    }
}
