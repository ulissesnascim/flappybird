﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashBackScript : MonoBehaviour {


    private SpriteRenderer spriteRenderer;
    GameObject gameController;
    ColumnPool columnPool;

    private bool flashBackActive;
    private bool effectApplied;
    private bool effectDisabled;

    // Use this for initialization
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        gameController = GameObject.Find("GameController");
        columnPool = gameController.GetComponent<ColumnPool>();
    }

    // Update is called once per frame
    void Update()
    {

        if (transform.position.x < -11f && flashBackActive == false)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        spriteRenderer.color = new Color(255, 255, 255, 0);
        StartCoroutine(FlashBackEffect());
        return;

    }

    private IEnumerator FlashBackEffect()
    {
        flashBackActive = true;

        if (!effectApplied)
        {
            GameController.instance.deltaDifficulty += 1;
            effectApplied = true;
        }

        yield return new WaitForSeconds(15);

        if (!effectDisabled)
        {
            Mathf.Max(0, GameController.instance.deltaDifficulty -= 1);
            effectDisabled = true;
        }

        flashBackActive = false;
    }

}