﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastForwardFlash : MonoBehaviour {

    public bool flashing;

    private int maxBlinks = 3;
    private float flashingLive = 0.7f;
    private int blinks = 0;
    private SpriteRenderer spriteRenderer;
    private int priorDifficulty = 1;
    private float timer = 0;


    // Use this for initialization
    void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

        if (GameController.instance.gameOver == true)
            flashing = false;



        if (blinks == maxBlinks)
        {
            spriteRenderer.color = new Color(255, 255, 255, 0);
            flashing = false;
            blinks = 0;
            timer = 0;
        }

        if (flashing == true)
        {
            timer += Time.deltaTime;

            if (timer < flashingLive)
            {
                spriteRenderer.color = new Color(255, 255, 255, 125);
            }
            else
            {
                spriteRenderer.color = new Color(255, 255, 255, 0);
                if (timer > 2 * flashingLive)
                {
                    timer = 0;
                    blinks += 1;
                }
            }



        }
        else
        {
            spriteRenderer.color = new Color(255, 255, 255, 0);
        }

    }
}
